const path = require("path");
const Dotenv = require('dotenv-webpack');

module.exports = {
    entry: "./server.js",
    output: {
      filename: "bundle.js"
    }, 
    watch: false,
    plugins: [
      new Dotenv()
    ]
  }