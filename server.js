import * as dotEnv from 'dotenv';
import express from 'express';
import { graphqlExpress, graphiqlExpress } from 'apollo-server-express';
import bodyParser from 'body-parser';
import cors from 'cors';
import schema from './data/schema';

dotEnv.config();

const EXPRESS_PORT = process.env.EXPRESS_PORT;
const app = express();

try{
  app.use('/graphql', bodyParser.json(), graphqlExpress({ schema }));
  app.use('/graphiql', graphiqlExpress({ endpointURL: '/graphql' }));
  app.use(cors);
  
  app.listen(EXPRESS_PORT, () =>
    console.log(
      `GraphiQL is now running 🚀 on http://localhost:${EXPRESS_PORT}/graphiql`
    )
  );
}catch(e){
  console.log(e);
}

